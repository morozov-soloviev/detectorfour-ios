//
//  RestApiBridge.swift
//  DetectorFour
//
//  Created by Ivan on 31/05/2019.
//  Copyright © 2019 MorozovSoloviev. All rights reserved.
//

import Foundation

class RestApi {
    
    static let host = "https://morozzz689.tplinkdns.com"
    static let port = "4051"
    
    static func loadMeasures() {
        var request = URLRequest(url: URL(string: host + ":" + port + "/api/measures")!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                var measuresList: [Measure] = try JSONDecoder().decode([Measure].self, from: data!)
            } catch {
                print("JSON Serialization error")
            }
        }).resume()
    }
    
    static func sendMeasure(measure: Measure) {
        var request = URLRequest(url: URL(string: host + ":" + port + "/api/measures")!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONEncoder().encode(measure)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
        }).resume()
        
    }
    
    static func deleteMeasure(measure: Measure) {
        var request = URLRequest(url: URL(string: host + ":" + port + "/api/measures/" + measure.Id)!)
        request.httpMethod = "DELETE"
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
        }).resume()
        
    }
}

