//
//  Measure.swift
//  DetectorFour
//
//  Created by Ivan on 31/05/2019.
//Copyright © 2019 MorozovSoloviev. All rights reserved.
//

import Foundation
import RealmSwift

class Measure: Object, Codable  {
    
    var Id: String = UUID().uuidString
    
    var Date: NSDate! = NSDate()
    
    var Name: String! = nil
    var Post: String! = nil
    var Mark: String! = nil
    var Model: String! = nil
    var Note: String! = nil
    
    var ValueLocationLatitude: Double! = nil
    var ValueLocationLongitude: Double! = nil
    
    var ValueMagneto_05: Double! = nil
    var ValueMagneto_1: Double! = nil
    var ValueMagneto_15: Double! = nil
    
    var Pressure: Double! = nil
    
    var Vibration: Double! = nil
    
    enum CodingKeys: String, CodingKey {
        case id
        case date
        case name
        case post
        case mark
        case model
        case note
        case valueLocationLatitude
        case valueLocationLongitude
        case valueMagneto_05
        case valueMagneto_1
        case valueMagneto_15
        case pressure
        case vibration
    }
    
    static let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.%sXXX"
    
    func encode(to encoder: Encoder) throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Measure.dateFormat
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(Id, forKey: .id)
        try container.encode(dateFormatter.string(from: Date as Date), forKey: .date)
        try container.encode(Name, forKey: .name)
        try container.encode(Post, forKey: .post)
        try container.encode(Mark, forKey: .mark)
        try container.encode(Model, forKey: .model)
        try container.encode(Note, forKey: .note)
        try container.encode(ValueLocationLatitude, forKey: .valueLocationLatitude)
        try container.encode(ValueLocationLongitude, forKey: .valueLocationLongitude)
        try container.encode(ValueMagneto_05, forKey: .valueMagneto_05)
        try container.encode(ValueMagneto_1, forKey: .valueMagneto_1)
        try container.encode(ValueMagneto_15, forKey: .valueMagneto_15)
        try container.encode(Pressure, forKey: .pressure)
        try container.encode(Vibration, forKey: .vibration)
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Measure.dateFormat
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        Id = try container.decode(String.self, forKey: .id)
        Date = dateFormatter.date(from: try container.decode(String.self, forKey: .date)) as NSDate?
        Name = try container.decode(String.self, forKey: .name)
        Post = try container.decode(String.self, forKey: .post)
        Mark = try container.decode(String.self, forKey: .mark)
        Model = try container.decode(String.self, forKey: .model)
        Note = try container.decode(String.self, forKey: .note)
        ValueLocationLatitude = try container.decode(Double.self, forKey: .valueLocationLatitude)
        ValueLocationLongitude = try container.decode(Double.self, forKey: .valueLocationLongitude)
        ValueMagneto_05 = try container.decode(Double.self, forKey: .valueMagneto_05)
        ValueMagneto_1 = try container.decode(Double.self, forKey: .valueMagneto_1)
        ValueMagneto_15 = try container.decode(Double.self, forKey: .valueMagneto_15)
        Pressure = try container.decode(Double.self, forKey: .pressure)
        Vibration = try container.decode(Double.self, forKey: .vibration)
    }
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
