//
//  ViewController.swift
//  DetectorFour
//
//  Created by Ivan on 17/05/2019.
//  Copyright © 2019 MorozovSoloviev. All rights reserved.
//

import UIKit
import SensingKit
import RealmSwift

class MainViewController: UIViewController {
    
    let sensingKit = SensingKitLib.shared()
    

    @IBOutlet var TextFieldLongtitude: UITextField!
    @IBOutlet var TextFieldLatitude: UITextField!
    @IBOutlet var TxtFieldMagneto_05: UITextField!
    @IBOutlet var TxtFieldMagneto_1: UITextField!
    @IBOutlet var TxtFieldMagneto_15: UITextField!
    @IBOutlet var TxtFieldVibration: UITextField!
    @IBOutlet var TxtFieldPressure: UITextField!
    
    @IBOutlet var LocationValueTxt: UILabel!
    @IBOutlet var MagnetoValueTxt: UILabel!
    @IBOutlet var VibrationValueTxt: UILabel!
    @IBOutlet var PressureValueTxt: UILabel!
    
    var Name: String? = ""
    var Post: String? = ""
    var Mark: String? = ""
    var Model: String? = ""
    var Note: String? = ""
    
    var ValueMomentAccelerometer: Double = 0.0//mgnovennye znachenia akselerometra
    var ValueStateAccelerometer: Double = 0.0//sohranennoe znachenia akselerometra
    
    var ValueMomentMagnetometer: Double = 0.0//mgnovennye znachenia magnetometra
    var ValueStateMagnetometer_05: Double = 0.0////sohranennoe znachenia magnetometra
    var ValueStateMagnetometer_1: Double = 0.0////sohranennoe znachenia magnetometra
    var ValueStateMagnetometer_15: Double = 0.0////sohranennoe znachenia magnetometra
    
    var ValueMomentLocLatitude: Double = 0.0//mgnovennye znachenia shiroty
    var ValueStateLocLatitude: Double = 0.0//sohranennoe znachenia shiroty
    
    var ValueMomentLocLongitude: Double = 0.0//mgnovennye znachenia dolgoty
    var ValueStateLocLongitude: Double = 0.0//sohranennoe znachenia dolgoty
    
    var ValueMomentPressure: Double = 0.0//mgnovennye znachenia davlenia
    var ValueStatePressure: Double = 0.0//sohranennoe znachenia davlenia
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //---------------ACCELEROMETER---------------------
        if sensingKit.isSensorAvailable(SKSensorType.Accelerometer) {//proverka na dostup datchika
            do {
                try sensingKit.register(SKSensorType.Accelerometer)
                do {//Подписаться на обработчик данных датчика.
                    try sensingKit.subscribe(to: SKSensorType.Accelerometer, withHandler: { (sensorType, sensorData, error) in
                        if (error == nil) {
                            let accData = sensorData as! SKAccelerometerData
                            let num = sqrt(accData.acceleration.x*accData.acceleration.x + accData.acceleration.y*accData.acceleration.y + accData.acceleration.z*accData.acceleration.z)
                            self.ValueMomentAccelerometer = num
                            self.VibrationValueTxt.text = "\(num) m/s^2"
                        }
                    })
                }
                catch {
                    // Handle error
                }
            }
            catch {
                // Handle error
            }
        }
        
        //---------------MAGNETOMETER---------------------
        if sensingKit.isSensorAvailable(SKSensorType.Magnetometer) {//proverka na dostup datchika
            do {
                try sensingKit.register(SKSensorType.Magnetometer)
                do {//Подписаться на обработчик данных датчика.
                    try sensingKit.subscribe(to: SKSensorType.Magnetometer, withHandler: { (sensorType, sensorData, error) in
                        if (error == nil) {
                            let magnData = sensorData as! SKMagnetometerData
                            let num = sqrt(magnData.magneticField.x*magnData.magneticField.x + magnData.magneticField.y*magnData.magneticField.y + magnData.magneticField.z*magnData.magneticField.z)
                            self.ValueMomentMagnetometer = num
                            self.MagnetoValueTxt.text = "\(num) mN"
                        }
                    })
                }
                catch {
                    // Handle error
                }
            }
            catch {
                // Handle error
            }
        }
        
        //---------------LOCATION---------------------
        if sensingKit.isSensorAvailable(SKSensorType.Location) {//proverka na dostup datchika
            do {
                try sensingKit.register(SKSensorType.Location)
                do {//Подписаться на обработчик данных датчика.
                    try sensingKit.subscribe(to: SKSensorType.Location, withHandler: { (sensorType, sensorData, error) in
                        if (error == nil) {
                            let locData = sensorData as! SKLocationData
                            let num1 = locData.location.coordinate.latitude
                            self.ValueMomentLocLatitude = num1
                            let num2 = locData.location.coordinate.longitude
                            self.ValueMomentLocLongitude = num2
                            self.LocationValueTxt.text = "\(num1) - \(num2)"
                        }
                    })
                }
                catch {
                    // Handle error
                }
            }
            catch {
                // Handle error
            }
        }
        
        //---------------ALTIMETER---------------------
        if sensingKit.isSensorAvailable(SKSensorType.Altimeter) {//proverka na dostup datchika
            do {
                try sensingKit.register(SKSensorType.Altimeter)
                do {//Подписаться на обработчик данных датчика.
                    try sensingKit.subscribe(to: SKSensorType.Altimeter, withHandler: { (sensorType, sensorData, error) in
                        if (error == nil) {
                            let altData = sensorData as! SKAltimeterData
                            let num = altData.altitudeData.pressure
                            self.ValueMomentPressure = Double(num)
                            self.PressureValueTxt.text = "\(num) hPa"
                        }
                    })
                }
                catch {
                    // Handle error
                }
            }
            catch {
                // Handle error
            }
        }
        
        do {
            try sensingKit.startContinuousSensingWithAllRegisteredSensors()
        }
        catch {
            // Handle error
        }
        
        
    }
    
    
    @IBAction func AddMeasure(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Новый замер", message: nil, preferredStyle: .alert)
        let addBtn = UIAlertAction(title: "Добавить", style: .default){(action) in
            let textFieldName = alert.textFields?[0].text
            self.Name = textFieldName
            
            let textFieldPost = alert.textFields?[1].text
            self.Post = textFieldPost
            
            let textFieldMark = alert.textFields?[2].text
            self.Mark = textFieldMark
            
            let textFieldModel = alert.textFields?[3].text
            self.Model = textFieldModel
            
            let textFieldNote = alert.textFields?[4].text
            self.Note = textFieldNote
            
            /*if(textFieldName != "" && textFieldPost != "" && textFieldMark != "" && textFieldModel != "" && textFieldNote != ""){*/
                /*let measure = Measure(Name: textFieldName , Post: textFieldPost, Mark: textFieldMark, Model: textFieldModel, Note: textFieldNote, ValueLocationLatitude: self.ValueStateLocLatitude, ValueLocationLongitude: self.ValueStateLocLongitude, ValueMagneto_05: self.ValueStateMagnetometer_05,
                                       ValueMagneto_1: self.ValueStateMagnetometer_1,ValueMagneto_15: self.ValueStateMagnetometer_15, Pressure: Double(truncating: self.ValueStatePressure), Vibration: self.ValueStateAccelerometer)*/
                
                //measure - объект для сохранения
            /*}
            else
            {
                let alertError = UIAlertController(title: "Внимание!", message: "Заполните все поля", preferredStyle: .alert)
                let cancelBtn = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alertError.addAction(cancelBtn)
                self.present(alertError, animated:true, completion:nil)
            }*/
            
            let uiRealm = try! Realm()
            
            var newMeasure: Measure! = Measure()
            newMeasure.Name = textFieldName
            newMeasure.Post = textFieldPost
            newMeasure.Mark = textFieldMark
            newMeasure.Model = textFieldModel
            newMeasure.Note = textFieldNote
            newMeasure.ValueLocationLatitude = self.ValueStateLocLatitude
            newMeasure.ValueLocationLongitude = self.ValueStateLocLongitude
            newMeasure.ValueMagneto_05 = self.ValueStateMagnetometer_05
            newMeasure.ValueMagneto_1 = self.ValueStateMagnetometer_1
            newMeasure.ValueMagneto_15 = self.ValueStateMagnetometer_15
            newMeasure.Pressure = self.ValueStatePressure
            newMeasure.Vibration = self.ValueStateAccelerometer
            
            try! uiRealm.write { () -> Void in
                uiRealm.add(newMeasure)
            }
            
            do {//test section
                var measures = Array(uiRealm.objects(Measure.self))
                
                var kek = RestApi.loadMeasures()
            }
        }
        
        
        
        let cancelBtn = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addTextField{(textFieldName) in textFieldName.placeholder = "Введите имя"}
        alert.addTextField{(textFieldPost) in textFieldPost.placeholder = "Введите должность"}
        alert.addTextField{(textFieldMark) in textFieldMark.placeholder = "Введите марку ЭВМ"}
        alert.addTextField{(textFieldModel) in textFieldModel.placeholder = "Введите модель ЭВМ"}
        alert.addTextField{(textFieldNote) in textFieldNote.placeholder = "Примечание"}
        
        alert.addAction(addBtn)
        alert.addAction(cancelBtn)
        present(alert,animated:true,completion:nil)
    }
    
    
    @IBAction func PressMagneto_05(_ sender: UIButton) {
        ValueStateMagnetometer_05 = ValueMomentMagnetometer
        TxtFieldMagneto_05.text = "\(ValueStateMagnetometer_05)"
    }
    
    @IBAction func PressMagneto_1(_ sender: UIButton) {
        ValueStateMagnetometer_1 = ValueMomentMagnetometer
        TxtFieldMagneto_1.text = "\(ValueStateMagnetometer_1)"
    }
    
    @IBAction func PressMagneto_15(_ sender: UIButton) {
        ValueStateMagnetometer_15 = ValueMomentMagnetometer
        TxtFieldMagneto_15.text = "\(ValueStateMagnetometer_15)"
    }
    
    @IBAction func PressVibration(_ sender: UIButton) {
        ValueStateAccelerometer = ValueMomentAccelerometer
        TxtFieldVibration.text = "\(ValueStateAccelerometer)"
    }
    
    @IBAction func PressLocation(_ sender: UIButton) {
        
        ValueStateLocLatitude = ValueMomentLocLatitude
        TextFieldLatitude.text = "\(ValueStateLocLatitude)"
        ValueStateLocLatitude = ValueMomentLocLongitude
        TextFieldLongtitude.text = "\(ValueStateLocLatitude)"
    }
    
    @IBAction func PressPressure(_ sender: UIButton) {
        ValueStatePressure = ValueMomentPressure
        TxtFieldPressure.text = "\(ValueStatePressure)"
    }
}

